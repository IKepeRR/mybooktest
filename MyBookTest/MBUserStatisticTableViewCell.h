//
//  MBUserStatisticTableViewCell.h
//  MyBookTest
//
//  Created by IKepeRR on 20.10.16.
//  Copyright © 2016 IKepeRR. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Niche;

@interface MBUserStatisticTableViewCell : UITableViewCell

- (void)setupCellWithNiche:(Niche *)niche andTotalBooksCount:(NSInteger)totalBooksCount;

@end
