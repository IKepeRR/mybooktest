//
//  MBColorConstants.h
//  MyBookTest
//
//  Created by IKepeRR on 20.10.16.
//  Copyright © 2016 IKepeRR. All rights reserved.
//

#define HumorNicheProgressViewColor     [UIColor colorWithRed:194.0/255.0 green:255.0/255.0 blue:0 alpha:1]
#define EroticNicheProgressViewColor    [UIColor colorWithRed:255.0/255.0 green:73.0/255.0 blue:111.0/255.0 alpha:1]
#define EzotericNicheProgressViewColor  [UIColor colorWithRed:225.0/255.0 green:130.0/255.0 blue:90.0/255.0 alpha:1]
#define FantasyNicheProgressViewColor   [UIColor colorWithRed:163.0/255.0 green:73.0/255.0 blue:193.0/255.0 alpha:1]
#define FantasticNicheProgressViewColor [UIColor colorWithRed:191.0/255.0 green:139.0/255.0 blue:235.0/255.0 alpha:1]
