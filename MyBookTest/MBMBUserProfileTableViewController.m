//
//  MBMBUserProfileTableViewController.m
//  MyBookTest
//
//  Created by IKepeRR on 20.10.16.
//  Copyright © 2016 IKepeRR. All rights reserved.
//

#import "MBMBUserProfileTableViewController.h"

#import "MBUserStatisticTableViewCell.h"

#import "MagicalRecord.h"
#import "MBNetwork.h"

#import "Niche.h"

@interface MBMBUserProfileTableViewController ()
{
    UIRefreshControl *refreshControl;
    
    NSArray *nichesArray;
    
    NSInteger totalBooksCount;
}
@end

@implementation MBMBUserProfileTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
    [self setupObjects];
}

#pragma mark - Navigation

#pragma mark - Delegates

#pragma mark <UITableViewDelegate>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return nichesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MBUserStatisticTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MBUserStatisticTableViewCell" forIndexPath:indexPath];
    
    [cell setupCellWithNiche:nichesArray[indexPath.row] andTotalBooksCount:totalBooksCount];
    
    return cell;
}

#pragma mark - Events

#pragma mark - Actions

- (void)setupUI
{
    self.tableView.tableFooterView = [UIView new];
    
    [self addRefreshControl];
}

- (void)setupObjects
{
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MBUserStatisticTableViewCell class]) bundle:nil] forCellReuseIdentifier:@"MBUserStatisticTableViewCell"];
    
    [self localStatistics];
    [self loadStatistics];
}

- (void)addRefreshControl
{
    refreshControl = [UIRefreshControl new];
    [refreshControl addTarget:self action:@selector(loadStatistics) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

- (void)loadStatistics
{
    [NETWORK userStatisticsWithCompletionBlock:^(id response, NSError *error)
     {
         [refreshControl endRefreshing];
         
         if (error == nil)
             [self localStatistics];
     }];
}

- (void)localStatistics
{
    nichesArray = [Niche MR_findAll];
    
    [self countTotalUsersBooks];
    [self adjustTableFooterView];
    [self sortNichesByBooksCount];
    [self.tableView reloadData];
}

- (void)adjustTableFooterView
{
    if (nichesArray.count == 0)
    {
        UILabel *noStatisticLabel = [[UILabel alloc] init];
        noStatisticLabel.text = @"\n\n\nСтатистика по жанрам появится,\nкак только вы начнете читать";
        noStatisticLabel.font = [UIFont fontWithName:@"HelveticaNeue-Regular" size:12];
        noStatisticLabel.textColor = [UIColor colorWithRed:135.0/255.0 green:139.0/255.0 blue:145.0/255.0 alpha:1];
        noStatisticLabel.numberOfLines = 0;
        noStatisticLabel.textAlignment = NSTextAlignmentCenter;
        [noStatisticLabel sizeToFit];
        
        self.tableView.tableFooterView = noStatisticLabel;
    }
    else
        self.tableView.tableFooterView = [UIView new];
}

- (void)countTotalUsersBooks
{
    totalBooksCount = 0;
    
    for (Niche *niche in nichesArray)
        totalBooksCount = totalBooksCount + niche.nowreadingCount.integerValue + niche.readCount.integerValue + niche.wishlistCount.integerValue;
}

- (void)sortNichesByBooksCount
{
    nichesArray = [nichesArray sortedArrayUsingComparator:^NSComparisonResult(Niche *niche1, Niche *niche2)
                   {
                       NSInteger booksInNiche1 = niche1.nowreadingCount.integerValue + niche1.readCount.integerValue + niche1.wishlistCount.integerValue;
                       NSInteger booksInNiche2 = niche2.nowreadingCount.integerValue + niche2.readCount.integerValue + niche2.wishlistCount.integerValue;
                       
                       if (booksInNiche1 >= booksInNiche2)
                           return NSOrderedAscending;
                       else
                           return NSOrderedDescending;
                   }];
}

@end
