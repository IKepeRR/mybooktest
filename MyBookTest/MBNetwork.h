//
//
//
//
//
//
//

#import <Foundation/Foundation.h>

#define NETWORK [MBNetwork sharedInstance]

typedef void(^responseBlock)(id response, NSError *error);

@interface MBNetwork : NSObject

+ (instancetype)sharedInstance;

- (void)userStatisticsWithCompletionBlock:(responseBlock)completion;

@end
