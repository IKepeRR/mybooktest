// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Niche.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface NicheID : NSManagedObjectID {}
@end

@interface _Niche : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) NicheID *objectID;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSNumber* nicheID;

@property (atomic) int32_t nicheIDValue;
- (int32_t)nicheIDValue;
- (void)setNicheIDValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* nowreadingCount;

@property (atomic) int32_t nowreadingCountValue;
- (int32_t)nowreadingCountValue;
- (void)setNowreadingCountValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* readCount;

@property (atomic) int32_t readCountValue;
- (int32_t)readCountValue;
- (void)setReadCountValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSString* slug;

@property (nonatomic, strong, nullable) NSNumber* wishlistCount;

@property (atomic) int32_t wishlistCountValue;
- (int32_t)wishlistCountValue;
- (void)setWishlistCountValue:(int32_t)value_;

@end

@interface _Niche (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveNicheID;
- (void)setPrimitiveNicheID:(NSNumber*)value;

- (int32_t)primitiveNicheIDValue;
- (void)setPrimitiveNicheIDValue:(int32_t)value_;

- (NSNumber*)primitiveNowreadingCount;
- (void)setPrimitiveNowreadingCount:(NSNumber*)value;

- (int32_t)primitiveNowreadingCountValue;
- (void)setPrimitiveNowreadingCountValue:(int32_t)value_;

- (NSNumber*)primitiveReadCount;
- (void)setPrimitiveReadCount:(NSNumber*)value;

- (int32_t)primitiveReadCountValue;
- (void)setPrimitiveReadCountValue:(int32_t)value_;

- (NSString*)primitiveSlug;
- (void)setPrimitiveSlug:(NSString*)value;

- (NSNumber*)primitiveWishlistCount;
- (void)setPrimitiveWishlistCount:(NSNumber*)value;

- (int32_t)primitiveWishlistCountValue;
- (void)setPrimitiveWishlistCountValue:(int32_t)value_;

@end

@interface NicheAttributes: NSObject 
+ (NSString *)name;
+ (NSString *)nicheID;
+ (NSString *)nowreadingCount;
+ (NSString *)readCount;
+ (NSString *)slug;
+ (NSString *)wishlistCount;
@end

NS_ASSUME_NONNULL_END
