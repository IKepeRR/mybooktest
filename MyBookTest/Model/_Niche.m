// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Niche.m instead.

#import "_Niche.h"

@implementation NicheID
@end

@implementation _Niche

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Niche" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Niche";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Niche" inManagedObjectContext:moc_];
}

- (NicheID*)objectID {
	return (NicheID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"nicheIDValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"nicheID"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"nowreadingCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"nowreadingCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"readCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"readCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"wishlistCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"wishlistCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic name;

@dynamic nicheID;

- (int32_t)nicheIDValue {
	NSNumber *result = [self nicheID];
	return [result intValue];
}

- (void)setNicheIDValue:(int32_t)value_ {
	[self setNicheID:@(value_)];
}

- (int32_t)primitiveNicheIDValue {
	NSNumber *result = [self primitiveNicheID];
	return [result intValue];
}

- (void)setPrimitiveNicheIDValue:(int32_t)value_ {
	[self setPrimitiveNicheID:@(value_)];
}

@dynamic nowreadingCount;

- (int32_t)nowreadingCountValue {
	NSNumber *result = [self nowreadingCount];
	return [result intValue];
}

- (void)setNowreadingCountValue:(int32_t)value_ {
	[self setNowreadingCount:@(value_)];
}

- (int32_t)primitiveNowreadingCountValue {
	NSNumber *result = [self primitiveNowreadingCount];
	return [result intValue];
}

- (void)setPrimitiveNowreadingCountValue:(int32_t)value_ {
	[self setPrimitiveNowreadingCount:@(value_)];
}

@dynamic readCount;

- (int32_t)readCountValue {
	NSNumber *result = [self readCount];
	return [result intValue];
}

- (void)setReadCountValue:(int32_t)value_ {
	[self setReadCount:@(value_)];
}

- (int32_t)primitiveReadCountValue {
	NSNumber *result = [self primitiveReadCount];
	return [result intValue];
}

- (void)setPrimitiveReadCountValue:(int32_t)value_ {
	[self setPrimitiveReadCount:@(value_)];
}

@dynamic slug;

@dynamic wishlistCount;

- (int32_t)wishlistCountValue {
	NSNumber *result = [self wishlistCount];
	return [result intValue];
}

- (void)setWishlistCountValue:(int32_t)value_ {
	[self setWishlistCount:@(value_)];
}

- (int32_t)primitiveWishlistCountValue {
	NSNumber *result = [self primitiveWishlistCount];
	return [result intValue];
}

- (void)setPrimitiveWishlistCountValue:(int32_t)value_ {
	[self setPrimitiveWishlistCount:@(value_)];
}

@end

@implementation NicheAttributes 
+ (NSString *)name {
	return @"name";
}
+ (NSString *)nicheID {
	return @"nicheID";
}
+ (NSString *)nowreadingCount {
	return @"nowreadingCount";
}
+ (NSString *)readCount {
	return @"readCount";
}
+ (NSString *)slug {
	return @"slug";
}
+ (NSString *)wishlistCount {
	return @"wishlistCount";
}
@end

