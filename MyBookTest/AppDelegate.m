//
//  AppDelegate.m
//  MyBookTest
//
//  Created by IKepeRR on 20.10.16.
//  Copyright © 2016 IKepeRR. All rights reserved.
//

#import "AppDelegate.h"

#import "MagicalRecord.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [MagicalRecord setLoggingLevel:MagicalRecordLoggingLevelOff];
    [MagicalRecord setupAutoMigratingCoreDataStack];
    
    return YES;
}

@end
