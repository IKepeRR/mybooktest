//
//  AppDelegate.h
//  MyBookTest
//
//  Created by IKepeRR on 20.10.16.
//  Copyright © 2016 IKepeRR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

