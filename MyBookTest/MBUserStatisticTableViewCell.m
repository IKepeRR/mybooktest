//
//  MBUserStatisticTableViewCell.m
//  MyBookTest
//
//  Created by IKepeRR on 20.10.16.
//  Copyright © 2016 IKepeRR. All rights reserved.
//

#import "MBUserStatisticTableViewCell.h"

#import "MBNicheColorConstants.h"
#import "MBNicheSlugConstants.h"

#import "Niche.h"

@interface MBUserStatisticTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *nicheImageView;
@property (weak, nonatomic) IBOutlet UILabel *nicheNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *wantToReadCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *readingCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *readCountLabel;

@property (weak, nonatomic) IBOutlet UIProgressView *statisticProgressView;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *bookTypeCountNameLabels;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *bookCountLabels;


@end

@implementation MBUserStatisticTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _statisticProgressView.trackImage = [UIImage imageNamed:@"progressBarEmpty"];
    _statisticProgressView.progressImage = [[[UIImage imageNamed:@"progressBarEmpty"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5) resizingMode:UIImageResizingModeStretch];
    
    if ([UIScreen mainScreen].bounds.size.width < 375)
        [self adjustFontsForSmallScreenSizes];
}

- (void)setupCellWithNiche:(Niche *)niche andTotalBooksCount:(NSInteger)totalBooksCount
{
    _nicheImageView.image = [UIImage imageNamed:[niche.slug stringByAppendingString:@"Icon"]];
    _nicheNameLabel.text = niche.name;
    _wantToReadCountLabel.text = niche.wishlistCount.stringValue;
    _readingCountLabel.text = niche.readCount.stringValue;
    _readCountLabel.text = niche.readCount.stringValue;
    
    [self adjustStatisticProgressViewColorForNiche:niche];
    
    _statisticProgressView.progress = (niche.readCount.floatValue + niche.readCount.floatValue + niche.wishlistCount.floatValue)/(float)totalBooksCount;
}

- (void)adjustStatisticProgressViewColorForNiche:(Niche *)niche
{
    _statisticProgressView.tintColor = [self statisticProgressViewColorForNicheWithSlug:niche.slug];
}

- (UIColor *)statisticProgressViewColorForNicheWithSlug:(NSString *)nicheSlug
{
    if ([nicheSlug isEqualToString:EroticSlug])
        return EroticNicheProgressViewColor;
    
    if ([nicheSlug isEqualToString:EzotericSlug])
        return EzotericNicheProgressViewColor;
    
    if ([nicheSlug isEqualToString:FantasticSlug])
        return FantasticNicheProgressViewColor;
    
    if ([nicheSlug isEqualToString:FantasySlug])
        return FantasyNicheProgressViewColor;
    
    if ([nicheSlug isEqualToString:HumorSlug])
        return HumorNicheProgressViewColor;
    
    
    return [UIColor new];
}

- (void)adjustFontsForSmallScreenSizes
{
    _nicheNameLabel.font = [_nicheNameLabel.font fontWithSize:16];
    
    for (UILabel *bookTypeCountNameLabel in _bookTypeCountNameLabels)
    {
        bookTypeCountNameLabel.font = [bookTypeCountNameLabel.font fontWithSize:12];
    }
    
    for (UILabel *bookCountLabel in _bookCountLabels)
    {
        bookCountLabel.font = [bookCountLabel.font fontWithSize:14];
    }
}
@end
