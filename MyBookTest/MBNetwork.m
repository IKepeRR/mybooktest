//
//
//
//
//
//
//

#import "MBNetwork.h"

#import "MagicalRecord.h"

#import "Niche.h"

NSString *const kBaseURLString = @"http://www.mocky.io/v2/57ff840b1300006c09cce2d8";

@interface MBNetwork () <NSURLSessionDelegate>

@end

@implementation MBNetwork

+ (instancetype)sharedInstance
{
    static MBNetwork *instance = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        instance = [[MBNetwork alloc] init];
    });
    
    return instance;
}

#pragma mark - User Profile

- (void)userStatisticsWithCompletionBlock:(responseBlock)completion
{
    [self requestWithMethodName:kBaseURLString completion:^(id response, NSError *error)
     {
         if (error == nil)
             [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext)
              {
                  [Niche MR_importFromArray: response[@"niche_stats"] inContext:localContext];
              } completion:^(BOOL contextDidSave, NSError * _Nullable error)
              {
                  completion(response, error);
              }];
         else
             completion(nil, error);
     }];
}

- (void)requestWithMethodName:(NSString*)method completion:(responseBlock)completion
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject
                                                                 delegate:self
                                                            delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURL *url = [NSURL URLWithString:method];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                                                        completion((data != nil)?[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]:nil, error);
                                                    }];
    [dataTask resume];
}

@end
